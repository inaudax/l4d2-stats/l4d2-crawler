::WEAPONS <- {};

/* function WEAPONS::byCode(code) {
  switch ( code ) {
    case "pumpshotgun":

  }
} */

function WEAPONS::initStats() {
  return {
    kills = 0,
    shots = 0,
    hits = 0,
    headshot_kills = 0
  };
}

/* 
  pipe_bomb: refers to explosions (propane tanks, oxygen tanks, etc) and shove kills using pipe bomb
  inferno: refers to molotov, gas can kills,
  molotov: refers to molotov shove kills
*/
function WEAPONS::buildStats() {
  local stats = {
    pumpshotgun = ::WEAPONS.initStats(),
    shotgun_chrome = ::WEAPONS.initStats(),
    smg_silenced = ::WEAPONS.initStats(),
    smg = ::WEAPONS.initStats(),
    smg_mp5 = ::WEAPONS.initStats(),
    autoshotgun = ::WEAPONS.initStats(),
    shotgun_spas = ::WEAPONS.initStats(),
    rifle_ak47 = ::WEAPONS.initStats(),
    rifle = ::WEAPONS.initStats(),
    rifle_desert = ::WEAPONS.initStats(),
    hunting_rifle = ::WEAPONS.initStats(),
    sniper_military = ::WEAPONS.initStats(),
    sniper_awp = ::WEAPONS.initStats(),
    sniper_scout = ::WEAPONS.initStats(),
    rifle_sg552 = ::WEAPONS.initStats(),
    grenade_launcher = ::WEAPONS.initStats(),
    grenade_launcher_projectile = null,
    rifle_m60 = ::WEAPONS.initStats(),
    pistol = ::WEAPONS.initStats(),
    dual_pistols = null,
    pistol_magnum = ::WEAPONS.initStats(),
    chainsaw = ::WEAPONS.initStats(),
    gnome = ::WEAPONS.initStats(),
    pipe_bomb = ::WEAPONS.initStats(),
    vomitjar = ::WEAPONS.initStats(),
    molotov = ::WEAPONS.initStats(),
    inferno = ::WEAPONS.initStats(),
    melee = ::WEAPONS.initStats(),
    fireworkcrate = ::WEAPONS.initStats(),
    fire_cracker_blast = null,
  };

  stats.dual_pistols = stats.pistol;
  stats.grenade_launcher_projectile = stats.grenade_launcher;
  stats.fire_cracker_blast = stats.fireworkcrate;

  return stats;
}
