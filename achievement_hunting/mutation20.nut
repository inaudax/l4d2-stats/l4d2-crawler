::GAME <- {
  god = true
};

function OnGameEvent_player_first_spawn(data) {
  printl("last gnome on earth");
  foreach (key, value in data) {
    printl(key + ": " + value);
  }
  printl("");

  local player = GetPlayerFromUserID(data.userid);

  if ( !IsPlayerABot(player) ) {
    return;
  }

  if ( player.IsSurvivor() ) {
    return;
  }

  if ( true || player.GetZombieType() == 2 ) {
    player.TakeDamage(100000, DMG_BULLET, player)
  }
}

function OnGameEvent_charger_charge_start(data) {
  local attacker = GetPlayerFromUserID(data.userid);

  attacker.TakeDamage(100000, DMG_BULLET, attacker);
}

function OnGameEvent_tongue_grab(data) {
 local attacker = GetPlayerFromUserID(data.userid);

  attacker.TakeDamage(100000, DMG_BULLET, attacker);
}

function OnGameEvent_jockey_ride(data) {
  local attacker = GetPlayerFromUserID(data.userid);

  attacker.TakeDamage(100000, DMG_BULLET, attacker);
}

function OnGameEvent_weapon_fire(data) {
  foreach (key, value in data) {
    printl(key + ": " + value);
  }


  local player = null;
  local attacker = GetPlayerFromUserID(data.userid);

  
  printl("");
  printl("Attacker: " + attacker.GetPlayerName());
  printl("");
  printl("");


  if ( attacker.IsPlayer() && !attacker.IsSurvivor() ) {
    attacker.TakeDamage(100000, DMG_BULLET, attacker);
    
    return;
  }

  if ( IsPlayerABot(attacker) ) {
    return;
  }

  while ( player = Entities.FindByClassname(player, "witch") ) {
    player.TakeDamage(100000, DMG_BULLET, attacker);
  }

  while ( player = Entities.FindByClassname(player, "infected") ) {
    player.TakeDamage(10000, DMG_BULLET, attacker);
  }

  if ( attacker.IsPlayer() && attacker.IsSurvivor() ) {
    attacker.GiveItem("ammo");
  }
}

function OnGameEvent_player_jump(data) {
  local player = GetPlayerFromUserID(data.userid);
  
  try {
    if ( player != null && player.IsSurvivor() && !IsPlayerABot(player) ) {
      player.UseAdrenaline(120);
      player.GiveItem("adrenaline");
      player.GiveItem("health");
      player.GiveItem("ammo");
    }

    
  }
  catch ( error ) {}
  
}

function AllowTakeDamage(data) {
  if ( !IsPlayerABot(data.Victim) && data.Victim.IsPlayer() && data.Victim.IsSurvivor() && ::GAME.god ) {
    return false;
  }

  return true;
}

function InterceptChat(message, player) {
  if ( message.find("fool") != null ) {
    ::GAME.god = false;
  }
  else if ( message.find("loof") != null ) {
    ::GAME.god = true;
  }
  else if ( message.find("defib") != null ) {
    player.GiveItem("defibrillator");
  }
  else if ( message.find("ded") != null ) {
    player.TakeDamage(10000, DMG_BULLET, player);
  }
  else if ( message.find("kill") != null ) {
    printl("DED");
    local survivor = null;

    while ( survivor = Entities.FindByClassname(survivor, "player") ) {
      if ( !IsPlayerABot(survivor) || !survivor.IsSurvivor() ) {
        continue;
      }
      
      /* survivor.TakeDamage(10000, DMG_BULLET, survivor); */
      survivor.TakeDamage(100000, DMG_BULLET, survivor);
    }

  }

  printl(::GAME.god);
}

function OnPostSpawn(data) {
  printl(data);
}