::GAME <- {
  god = true,
  autokill_common = true,
  autokill_special = true,
  autoreload = true
};

::PLAYER_OPTIONS <- {};

function CreatePlayerOptions() {
  local stats = {
    ammo_explode = false,
    ammo_fire = false
  };

  return stats;
}

function GetPlayerOptions(player) {
  local steamId = player.GetNetworkIDString();

  if ( !(steamId in ::PLAYER_OPTIONS) ) {
    ::PLAYER_OPTIONS[steamId] <- CreatePlayerOptions();
  }

  return ::PLAYER_OPTIONS[steamId];
}

function GiveWeaponUpgrades(isEnabled, player) {
  if ( !isEnabled ) {
    return;
  }

  local playerOptions = GetPlayerOptions(player);

  if ( playerOptions.ammo_explode ) {
    player.GiveUpgrade(UPGRADE_EXPLOSIVE_AMMO);
  }
  else if ( playerOptions.ammo_fire ) {
    player.GiveUpgrade(UPGRADE_INCENDIARY_AMMO);
  }
}

function KillPlayer(isEnabled, player, attacker = null) {
  if ( !isEnabled ) {
    return;
  }

  if ( attacker == null ) {
    attacker = player;
  }

  player.TakeDamage(100000, DMG_BULLET, attacker);
}

function KillWitches(isEnabled, attacker = null) {
  if ( isEnabled && ::GAME.autokill_special ) {
    local player = null;

    while ( player = Entities.FindByClassname(player, "witch") ) {
      if ( attacker == null ) {
        attacker = player;
      }

      KillPlayer(true, player, attacker);
    }
  }
}

function IsSpecialInfected(player) {
  if ( player == null ) {
    return false;
  }

  return ( player.IsPlayer() && !player.IsSurvivor() );
}

function IsSurvivor(player) {
  if ( player == null ) {
    return false;
  }

  return ( player.IsPlayer() && player.IsSurvivor() );
}

function KillAllSpecialInfected(isEnabled, attacker = null) {
  if ( !isEnabled ) {
    return;
  }

  local infected = null;

  while ( infected = Entities.FindByClassname(infected, "player") ) {
    if ( !IsPlayerABot(infected) || infected.IsSurvivor() ) {
      continue;
    }

    if ( attacker == null ) {
      attacker = infected;
    }

    KillPlayer(true, infected, attacker);
    KillPlayer(true, infected, attacker);
  }
}

function KillSpecialInfected(isEnabled, player, zombieType) {
  if ( !isEnabled || !IsSpecialInfected(player) || zombieType != player.GetZombieType() ) {
    return;
  }

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, player);
  }
}

function KillCommonInfected(isEnabled, attacker) {
  if ( isEnabled && ::GAME.autokill_common ) {
    local player = null;

    while ( player = Entities.FindByClassname(player, "infected") ) {
      KillPlayer(true, player, attacker);
    }
  }
}

function KillBotSurvivors(isEnabled) {
  if ( !isEnabled ) {
    return;
  }

  local survivor = null;

  while ( survivor = Entities.FindByClassname(survivor, "player") ) {
    if ( !IsPlayerABot(survivor) || !survivor.IsSurvivor() ) {
      continue;
    }

    KillPlayer(true, survivor);
    KillPlayer(true, survivor);
  }
}

function AutoReloadWeapon(isEnabled, player) {
  if ( player.IsPlayer() && player.IsSurvivor() ) {
    player.GiveItem("ammo");

    if ( ::GAME.autoreload ) {
      local weapon = player.GetActiveWeapon();

      try {
        if ( weapon != null ) {
          weapon.GiveDefaultAmmo();
        }
      }
      catch ( error ) {
        printl("Error autoreloading weapon.");
      }
    }
  }
}

function GiveItem(isEnabled, item, player) {
  if ( !isEnabled ) {
    return;
  }

  try {
    if ( item == "vomitjar" ) {
      for ( local i = 1; i <= 20; i+= 1 ) {
        player.GiveItem("molotov");
        player.GiveItem(item);
      }
    }
    else if ( item == "gascan" ) {
      for ( local i = 1; i <= 20; i+= 1 ) {
        player.GiveItem("propanetank");
        player.GiveItem(item);
      }
    }
    else {
      player.GiveItem(item);
    }
  }
  catch ( error ) {
    printl("Error giving item.");
  }
}

function AllowTakeDamage(data) {
  if ( !IsPlayerABot(data.Victim) && IsSurvivor(data.Victim) && ::GAME.god ) {
    return false;
  }

  return true;
}

function OnGameEvent_player_jump(data) {
  local player = GetPlayerFromUserID(data.userid);

  try {
    if ( player != null && player.IsSurvivor() && !IsPlayerABot(player) ) {
      player.GiveItem("adrenaline");
      player.GiveItem("health");
      player.GiveItem("ammo");

      Director.ResetMobTimer();
      Director.ResetSpecialTimers();
    }
  }
  catch ( error ) {}
}

function PrintTable(table) {
  foreach (key, value in table) {
    printl(key + ": " + value);
  }
}

function InterceptChat(message, player) {
  if ( IsPlayerABot(player) ) {
    return;
  }

  local playerOptions = GetPlayerOptions(player);

  if ( message.find("god 0") != null ) {
    ::GAME.god = false;
  }
  else if ( message.find("god 1") != null ) {
    ::GAME.god = true;
  }
  else if ( message.find("defib") != null ) {
    player.GiveItem("defibrillator");
  }
  else if ( message.find("ded") != null ) {
    KillPlayer(true, player);
  }
  else if ( message.find("nope") != null) {
    KillAllSpecialInfected(true, player);
  }
  else if ( message.find("kill") != null ) {
    KillBotSurvivors(true);
  }
  else if ( message.find("akc 0") != null ) {
    printl("autokill_common = false");
    ::GAME.autokill_common = false;
  }
  else if ( message.find("akc 1") != null ) {
    printl("autokill_common = true");
    ::GAME.autokill_common = true;
  }
  else if ( message.find("aks 0") != null ) {
    printl("autokill_special = false");
    ::GAME.autokill_special = false;
  }
  else if ( message.find("aks 1") != null ) {
    printl("autokill_special = true");
    ::GAME.autokill_special = true;
  }
  else if ( message.find("ar 0") != null ) {
    printl("autoreload = false");
    ::GAME.autoreload = false;
  }
  else if ( message.find("ar 1") != null ) {
    printl("autoreload = true");
    ::GAME.autoreload = true;
  }
  else if ( message.find("adr 0") != null ) {
    player.UseAdrenaline(0);
  }
  else if ( message.find("adr 1") != null ) {
    player.UseAdrenaline(3600);
  }
  else if ( message.find("give ") != null ) {
    local item = strip(message.slice(message.find("give") + 5));

    GiveItem(true, item, player);
  }
  else if ( message.find("behold") != null ) {
    player.ReviveByDefib();
  }
  else if ( message.find("laser 0") != null ) {
    player.RemoveUpgrade(UPGRADE_LASER_SIGHT);
  }
  else if ( message.find("laser 1") != null ) {
    player.GiveUpgrade(UPGRADE_LASER_SIGHT);
  }
  else if ( message.find("upg_e 0") != null ) {
    playerOptions.ammo_explode = false;

    player.RemoveUpgrade(UPGRADE_EXPLOSIVE_AMMO);
  }
  else if ( message.find("upg_e 1") != null ) {
    playerOptions.ammo_explode = true;
    playerOptions.ammo_fire = false;

    player.RemoveUpgrade(UPGRADE_INCENDIARY_AMMO);
    player.GiveUpgrade(UPGRADE_EXPLOSIVE_AMMO);
  }
  else if ( message.find("upg_i 0") != null ) {
    playerOptions.ammo_fire = false;

    player.RemoveUpgrade(UPGRADE_INCENDIARY_AMMO);
  }
  else if ( message.find("upg_i 1") != null ) {
    playerOptions.ammo_explode = false;
    playerOptions.ammo_fire = true;

    player.RemoveUpgrade(UPGRADE_EXPLOSIVE_AMMO);
    player.GiveUpgrade(UPGRADE_INCENDIARY_AMMO);
  }
}
