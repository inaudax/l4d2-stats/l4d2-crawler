IncludeScript("game.nut");

function OnGameEvent_player_first_spawn(data) {
  local player = GetPlayerFromUserID(data.userid);

  if ( !IsPlayerABot(player) ) {
    return;
  }

  if ( player.IsSurvivor() ) {
    return;
  }

  KillSpecialInfected(true, player, 1);
  KillSpecialInfected(true, player, 2);
  KillSpecialInfected(true, player, 3);
  KillSpecialInfected(true, player, 4);
  KillSpecialInfected(true, player, 5);
  KillSpecialInfected(true, player, 6);
  KillSpecialInfected(true, player, 8);
}

function OnGameEvent_charger_charge_start(data) {
  local attacker = GetPlayerFromUserID(data.userid);

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, attacker);
  }
}

function OnGameEvent_tongue_grab(data) {
  local attacker = GetPlayerFromUserID(data.userid);
  local victim = GetPlayerFromUserID(data.victim);

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, attacker, victim);
  }
}

function OnGameEvent_jockey_ride(data) {
  local attacker = GetPlayerFromUserID(data.userid);
  local victim = GetPlayerFromUserID(data.victim);

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, attacker, victim);
  }
}

function OnGameEvent_weapon_fire(data) {
  local player = null;
  local attacker = GetPlayerFromUserID(data.userid);

  if ( IsPlayerABot(attacker) ) {
    KillSpecialInfected(true, attacker, 1);
    KillSpecialInfected(true, attacker, 2);
    KillSpecialInfected(true, attacker, 3);
    KillSpecialInfected(true, attacker, 4);
    KillSpecialInfected(true, attacker, 5);
    KillSpecialInfected(true, attacker, 6);
    KillSpecialInfected(true, attacker, 8);

    return;
  }

  GiveWeaponUpgrades(true, attacker);
  KillWitches(true, attacker);
  KillCommonInfected(true, attacker);
  AutoReloadWeapon(true, attacker);
}

function OnPostSpawn(data) {
  printl(data);
}
