IncludeScript("game.nut");

function OnGameEvent_player_first_spawn(data) {
  local player = GetPlayerFromUserID(data.userid);

  if ( !IsPlayerABot(player) ) {
    return;
  }

  if ( player.IsSurvivor() ) {
    return;
  }

  KillSpecialInfected(false, player, 1);
  KillSpecialInfected(true, player, 2);
  KillSpecialInfected(false, player, 3);
  KillSpecialInfected(false, player, 4);
  KillSpecialInfected(false, player, 5);
  KillSpecialInfected(false, player, 6);
  KillSpecialInfected(false, player, 8);
}

function OnGameEvent_charger_charge_start(data) {
  local attacker = GetPlayerFromUserID(data.userid);

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, attacker);
  }
}

function OnGameEvent_tongue_grab(data) {
  local attacker = GetPlayerFromUserID(data.userid);
  local victim = GetPlayerFromUserID(data.victim);

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, attacker, victim);
  }
}

function OnGameEvent_jockey_ride(data) {
  local attacker = GetPlayerFromUserID(data.userid);
  local victim = GetPlayerFromUserID(data.victim);

  if ( ::GAME.autokill_special ) {
    KillPlayer(true, attacker, victim);
  }
}

function OnGameEvent_weapon_fire(data) {
  local attacker = GetPlayerFromUserID(data.userid);

  if ( IsPlayerABot(attacker) ) {
    KillSpecialInfected(false, attacker, 1);
    KillSpecialInfected(true, attacker, 2);
    KillSpecialInfected(false, attacker, 3);
    KillSpecialInfected(true, attacker, 4);
    KillSpecialInfected(false, attacker, 5);
    KillSpecialInfected(false, attacker, 6);
    KillSpecialInfected(true, attacker, 8);

    return;
  }

  GiveWeaponUpgrades(true, attacker);
  KillWitches(true, attacker);
  KillCommonInfected(true, attacker);
  AutoReloadWeapon(true, attacker);
}

function OnPostSpawn(data) {
  printl(data);
}
