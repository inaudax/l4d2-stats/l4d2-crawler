### l4d2-crawler

Scripts for crawling Left 4 Dead 2 stats.

#### Game Info
- Random match code
- Finale map
- Game mode
- Difficulty
- Duration
- Retries


#### Stats (tracked per player)

- Witch damage
- Witches crowned
- Witches startled
- Total deaths
- Death caused by self
- Total common infected killed
- Total smokers killed
- Total boomers killed
- Total hunters killed
- Total spitters killed
- Total jockeys killed
- Total chargers killed
- Total witches killed (last hit, basically)
- Total tanks killed (last hit, basically)
- Tank damage
- Headshot kills
- Total shots fired (including melee; shotguns count per pellet)
- Total shots landed (including melee; shotguns count per pellet)
- Total damage taken
- Total times incapacitated
- Total times player killed a boomer and exploded on team
- Total times a boomer puked on player
- Total times a boomer exploded on player
- Total times pulled by a smoker
- Total times choked by a smoker
- Total times stopped a smoker pull
- Total times stopped a smoker choke
- Total times pounced by a hunter
- Total times stopped a hunter pounce
- Total times ridden by a jockey
- Total times killed a riding jockey
- Total times rammed by a charger
- Total times hit by a ramming charger
- Total times pummelled by a charge
- Total times killed a charging charger
- Total incendiary packs used
- Total explosive packs used
- Total laser sights used
- Total incendiary packs deployed
- Total explosive packs deployed
- Times incapapacitated by common infected
- Times incapapacitated by smoker
- Times incapapacitated by boomer
- Times incapapacitated by hunter
- Times incapapacitated by spitter
- Times incapapacitated by jockey
- Times incapapacitated by charger
- Times incapapacitated by witch
- Times incapapacitated by tank
- Times incapapacitated by teammates
- Times incapapacitated by self
- Total times healed self
- Total times healed teammates
- Total times healed by teammates
- Total friendly fire damage
- Total damage taken by friendly fire
- Times friendly fired
- Total pain pills given to teammates
- Total pain pills used
- Total adrenaline shots given to teammates
- Total adrenaline shots used
- Total times revived teammates
- Total times used defibrillator on teammates
- Total times defibrillator used on player
- Total times protected teammates
- Total times protected by teammates
- Total times saved teammates
- Total times saved by teammates
- Survived campaign (dead or alive after finale)
- Disconnected (wasn't able to finish campaign; ignored for bots)
- Is human or bot
- Character used
- Username during game
- Weapon stats
  - Total shots fired (including melee; shotguns count per pellet)
  - Total shots landed (including melee; shotguns count per pellet)
  - Total kills
  - Total headshot kills
  - Notes
    - Only actual weapons are included
      - Stats for molotovs, pipe bombs, gas cans, gas tanks, etc., are inaccurate except for shots fired
      - Gas tanks and oxygen tanks explosions are considered as pipe bomb kills
      - Flame-based weapon kills are attributed to "inferno" (except firework boxes) regardless of actual weapon used. E.g. molotovs and gas can kills are attributed to "inferno" instead of their own
      - Incendiary and explosive ammo upgrades still count toward the weapon used
      - Kills by shoving using non-weapons are not counted (they still count for total kills, not just for weapon stats)
    - Special infected kill by shoving and flames are not attributed to the weapon used
    - All melee weapons are considered as one weapon called "melee"
    - Kills by shoving are attributed to the actual weapons used. E.g. molotovs, gas cans
    - Shotgun pellets landed are reduced to 2 when firing with an explosive ammo pack. However, the number of pellets counted as fired is still the same as the original value. E.g a combat shotgun fires 9 pellets, if an explosive pack is used, only a maximum of 2 pellets are counted as landed, but still counts 9 for the number of pellets fired. So explosive ammo packs basically reduce accuracy for shotguns


