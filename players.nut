::PLAYERS <- {};

function getPlayerData(playerId, isConnecting = false) {
  local player = GetPlayerFromUserID(playerId);
  
  if ( player == null ) {
    return null;
  }

  /* if ( IsPlayerABot(player) ) {
    return null;
  } */

  if ( !player.IsSurvivor() && !isConnecting ) {
    return null;
  }

  local steamId = player.GetNetworkIDString();

  if ( IsPlayerABot(player) ) {
    steamId = player.GetPlayerName();
  }

  if ( !(steamId in ::PLAYERS) ) {
    ::PLAYERS[steamId] <- {
      player = player,
      witch_damage = 0,
      witch_crown = 0,
      witch_startled = 0,
      deaths = 0,
      deaths_suicide = 0,
      kills_common_infected = 0,
      kills_smoker = 0,
      kills_boomer = 0,
      kills_hunter = 0,
      kills_spitter = 0,
      kills_jockey = 0,
      kills_charger = 0,
      kills_survivor = 0,
      kills_witch = 0,
      kills_tank = 0,
      tank_damage = 0,
      headshot_kills = 0,
      weapon_shots = 0,
      weapon_hits = 0,
      damage_taken = 0,
      times_incapacitated = 0,
      triggered_boomer_splash = 0,
      puked_by_boomer = 0,
      exploded_by_boomer = 0,
      pulled_by_smoker = 0,
      choked_by_smoker = 0,
      stopped_smoker_pull = 0,
      stopped_smoker_choke = 0,
      pounced_by_hunter = 0,
      stopped_hunter_pounce = 0,
      rode_by_jockey = 0,
      killed_riding_jockey = 0,
      rammed_by_charger = 0,
      hit_by_charger = 0,
      pummelled_by_charger = 0,
      killed_charging_charger = 0,
      upgrades = {
        incendiary = 0,
        explosive = 0,
        laser = 0
      },
      deployed_incendiary = 0,
      deployed_explosive = 0,
      incapacitators = {
        common_infected = 0,
        smoker = 0,
        boomer = 0,
        hunter = 0,
        spitter = 0,
        jockey = 0,
        charger = 0,
        witch = 0,
        tank = 0,
        team = 0,
        self = 0
      },
      heal_self = 0,
      heal_for_team = 0,
      heal_from_team = 0,
      damage_friendly_fire = 0,
      damage_friendly_fire_taken = 0,
      times_friendly_fire = 0,
      pain_pills_given = 0,
      pain_pills_used = 0,
      adrenaline_given = 0,
      adrenaline_used = 0,
      revived_team = 0,
      defib_for_team = 0,
      defib_from_team = 0,
      protected_team = 0,
      protected_by_team = 0,
      saved_team = 0,
      saved_by_team = 0,
      survived = false,
      disconnected = true,
      is_human = false,
      character = "",
      username = "",
      weapons = ::WEAPONS.buildStats()
    };
  }

  ::PLAYERS[steamId].character = GetCharacterDisplayName(player);
  ::PLAYERS[steamId].username = player.GetPlayerName();
  ::PLAYERS[steamId].player = player;

  return ::PLAYERS[steamId];
}