IncludeScript("weapons.nut");
IncludeScript("players.nut");

/* duration is in seconds */
::GAME <- {
  retries = 0,
  map = "",
  difficulty = 0,
  game_mode = "",
  duration = 0,
  is_realism = true,
  difficulty_ratio = {
    diff_0 = 0,
    diff_1 = 0,
    diff_2 = 0,
    diff_3 = 0
  },
  last_difficulty_change_time = 0
};

::SAVE_KEYS <- {
  PLAYERS = "SAVE_PLAYER_DATA",
  GAME = "GAME_DATA"
};

/* common infected kills and witches */
/* no weapon registered for shoving and props (like gas cans and propane tanks) */
function OnGameEvent_infected_death(data) {
  local playerData = getPlayerData(data.attacker);

  if ( playerData == null ) {
    return;
  }

  playerData.kills_common_infected++;
}

/* witch damage */
function OnGameEvent_infected_hurt(data) {
  local infected = EntIndexToHScript(data.entityid);
  local playerData = getPlayerData(data.attacker);

  if ( infected.GetClassname() != "witch" || playerData == null ) {
    return;
  }

  playerData.witch_damage += data.amount;
}

/* witch startled */
function OnGameEvent_witch_harasser_set(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.witch_startled++;
}

/* witch killed */
function OnGameEvent_witch_killed(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  if ( data.oneshot ) {
    playerData.witch_crown++;
  }

  playerData.kills_witch++;

  /* reduce total common infected killed by 1 (because witches count as common infected) */
  if ( playerData.kills_common_infected > 0 ) {
    playerData.kills_common_infected--;
  }
}

/* special infected kills or player deaths */
function OnGameEvent_player_death(data) {
  local victimData = null;
  local killerData = null;

  if ( "userid" in data ) {
    victimData = getPlayerData(data.userid);
  }

  if ( "attacker" in data ) {
    killerData = getPlayerData(data.attacker);
  }

  /* if victim and killer are not a player */
  if ( victimData == null && killerData == null ) {
    return;
  }

  /* player died */
  if ( victimData != null ) {
    victimData.deaths++;
  }

  /* player suicide */
  if ( victimData != null && killerData != null && data.userid == data.attacker ) {
    victimData.deaths_suicide++;
    return;
  }

  if ( killerData == null ) {
    return;
  }

  /* player killed an infected */
  try {
    killerData.weapons[data.weapon].kills++;

    if ( data.headshot ) {
      killerData.headshot_kills++;
      killerData.weapons[data.weapon].headshot_kills++;
    }
  }
  catch ( exception ) {
    printl("Invalid weapon. [" + data.weapon + "]");
  }

  /* if common infected/witch */
  if ( !("userid" in data) ) {
    return;
  }

  /* special infected killed or another player killed by player */
  /* molotovs don't work */
  local specialInfected = GetPlayerFromUserID(data.userid);

  switch ( specialInfected.GetZombieType() ) {
    case 1: killerData.kills_smoker++; break;
    case 2: killerData.kills_boomer++; break;
    case 3: killerData.kills_hunter++; break;
    case 4: killerData.kills_spitter++; break;
    case 5: killerData.kills_jockey++; break;
    case 6: killerData.kills_charger++; break;
    case 8: killerData.kills_tank++; break;
    case 9: killerData.kills_survivor++; break;
  }

  announceLastHitStatus(killerData.player, specialInfected);
}

/* tank/player damage */
function OnGameEvent_player_hurt(data) {
  local playerData = getPlayerData(data.userid);
  local totalDamage = 0;

  if ( "dmg_health" in data ) {
    totalDamage += data.dmg_health;
  }

  if ( "dmg_armor" in data ) {
    totalDamage += data.dmg_armor;
  }

  /* friendly fire damage */
  if ( ("attacker" in data) && data.userid != data.attacker ) {
    local attackerData = getPlayerData(data.attacker);

    if ( attackerData != null && playerData != null ) {
      attackerData.damage_friendly_fire += totalDamage;
      playerData.damage_friendly_fire_taken += totalDamage;
    }
  }

  /* player was damaged */
  if ( playerData != null ) {
    playerData.damage_taken += totalDamage;

    return;
  }

  /* tank damage */
  local infected = GetPlayerFromUserID(data.userid);

  if (
    infected == null ||
    infected.GetZombieType() != 8 ||
    !("attacker" in data)
  ) {
    return;
  }

  local playerData = getPlayerData(data.attacker);

  if ( playerData == null ) {
    return;
  }

  if ( "dmg_health" in data ) {
    playerData.tank_damage += data.dmg_health;
  }

  if ( "dmg_armor" in data ) {
    playerData.tank_damage += data.dmg_armor;
  }
}

/* shots */
function OnGameEvent_weapon_fire(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  try {
    local count = 1;

    if ( "count" in data ) {
      count = data.count;
    }

    playerData.weapons[data.weapon].shots += count;
    playerData.weapon_shots += count;
  }
  catch ( exception ) {
    printl("Invalid weapon fired [" + data.weapon + "]");
  }
}

::DAMAGE_COUNTER <- {};

function displayTable(data) {
  foreach ( key, value in data ) {
    printl(key + ": " + value);
  }
}

function addDamageDealtToVictim(attacker, victim, damageDone) {
  local victimHealth = victim.GetHealth();
  local victimId = victim.GetPlayerUserId();
  local attackerId = attacker.GetPlayerUserId();

  if ( victimHealth < damageDone ) {
    damageDone = victimHealth;
  }

  if ( !(victimId in ::DAMAGE_COUNTER) ) {
    ::DAMAGE_COUNTER[victimId] <- {};
  }

  if ( !(attackerId in ::DAMAGE_COUNTER[victimId]) ) {
    ::DAMAGE_COUNTER[victimId][attackerId] <- {
      damage = 0,
      hits = 0
    };
  }

  ::DAMAGE_COUNTER[victimId][attackerId].damage += damageDone;

  if ( damageDone > 0 ) {
    ::DAMAGE_COUNTER[victimId][attackerId].hits++;
  }
}

function sortDamageArray(damageData1, damageData2) {
  return  damageData2.damage - damageData1.damage;
}

function getSpecialInfectedName(infected) {
  switch ( infected.GetZombieType() ) {
    case 1: return "Smoker";
    case 2: return "Boomer";
    case 3: return "Hunter";
    case 4: return "Spitter";
    case 5: return "Jockey";
    case 6: return "Charger";
    case 8: return "Tank";
    default: return "Special"
  }
}

function announceOneHitSpecialKill(attacker, victim) {
  local specialInfectedName = getSpecialInfectedName(victim);
  local message = "Killed " + specialInfectedName + " in one shot";
  Say(attacker, message, false);
}

function announceLastHitStatus(attacker, victim) {
  local attackerId = attacker.GetPlayerUserId();
  local victimId = victim.GetPlayerUserId();
  local totalDamageTaken = 0;
  local damageArray = [];

  if ( !(victimId in ::DAMAGE_COUNTER) ) {
    return;
  }

  if ( victimId in ::DAMAGE_COUNTER && ::DAMAGE_COUNTER[victimId].len() == 1 && attackerId in ::DAMAGE_COUNTER[victimId] && ::DAMAGE_COUNTER[victimId][attackerId].hits == 1 && victim.GetZombieType() != 2) {
    return announceOneHitSpecialKill(attacker, victim);
  } else if (::DAMAGE_COUNTER[victimId].len() == 1 ) {
    return;
  }

  foreach ( key, value in ::DAMAGE_COUNTER[victimId] ) {
    local damageData = {
      attackerId = key,
      damage = value.damage,
      hits = value.hits
    };

    totalDamageTaken += value.damage;
    damageArray.append(damageData);
  }

  damageArray.sort(sortDamageArray);

  local specialInfectedName = getSpecialInfectedName(victim);
  local message = "Killed " + specialInfectedName + "\n";

  foreach ( index, damageData in damageArray ) {
    local survivor = GetPlayerFromUserID(damageData.attackerId);
    local damagePercent = ((damageData.damage / totalDamageTaken) * 100).tointeger();
    local pluralSuffix = (damageData.hits == 1 ? "" : "s");

    message += (" " + (index + 1) + ") " + survivor.GetPlayerName() + ": " + damagePercent + "% / " + damageData.hits  + " hit" + pluralSuffix + "\n");
  }

  message += "\n\n";

  ::DAMAGE_COUNTER.rawdelete(victimId);

  Say(attacker, message, false);
}

/* for accuracy */
function AllowTakeDamage(data) {
  if ( data.DamageDone < 1 ) {
    return true;
  }

  local attacker = data.Attacker;
  local weapon = data.Weapon;
  local victim = data.Victim;

  if ( attacker == null || !attacker.IsPlayer() || weapon == null) {
    return true;
  }

  if ( victim != null && victim.IsPlayer() ) {
    addDamageDealtToVictim(attacker, victim, data.DamageDone);
  }

  local playerData = getPlayerData(attacker.GetPlayerUserId());
  local weaponName = weapon.GetClassname().slice(7);

  if ( playerData == null ) {
    return true;
  }

  try {
    playerData.weapons[weaponName].hits++;
    playerData.weapon_hits++;
  }
  catch ( exception ) {
    printl("Invalid weapon name for hit [" + weapon.GetClassname() + "]");
    throw error;
  }

  return true;
}

/* for incapacitated */
function OnGameEvent_player_incapacitated(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.times_incapacitated++;

  /* witch or common infected */
  if ( "attackerentid" in data ) {
    local infected = EntIndexToHScript(data.attackerentid);
    local infectedName = infected.GetClassname();

    if ( infectedName == "witch" ) {
      playerData.incapacitators.witch++;
    }
    else if ( infectedName == "infected" ) {
      playerData.incapacitators.common_infected++;
    }

    return;
  }

  local attacker = GetPlayerFromUserID(data.attacker);

  if ( attacker == null ) {
    return;
  }

  /* incapacitated self */
  if ( attacker.GetPlayerUserId() == playerData.player.GetPlayerUserId() ) {
    playerData.incapacitators.self++;
    return;
  }

  /* incapacitated by team or special infected */
  switch ( attacker.GetZombieType() ) {
    case 1: playerData.incapacitators.smoker++; break;
    case 2: playerData.incapacitators.boomer++; break;
    case 3: playerData.incapacitators.hunter++; break;
    case 4: playerData.incapacitators.spitter++; break;
    case 5: playerData.incapacitators.jockey++; break;
    case 6: playerData.incapacitators.charger++; break;
    case 8: playerData.incapacitators.tank++; break;
    case 9: playerData.incapacitators.team++; break;
  }

}

/* first aid stats */
function OnGameEvent_heal_success(data) {
  local healerData = getPlayerData(data.userid);
  local targetData = getPlayerData(data.subject);

  /* player healed self */
  if ( data.userid == data.subject ) {
    healerData.heal_self++;

    return;
  }

  if ( healerData != null ) {
    healerData.heal_for_team++;
  }

  if ( targetData != null ) {
    targetData.heal_from_team++;
  }
}

/* friendly fire */
function OnGameEvent_friendly_fire(data) {
  local attackerData = getPlayerData(data.attacker);

  if ( attackerData != null ) {
    attackerData.times_friendly_fire++;
  }
}

/* pills */
function OnGameEvent_pills_used(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.pain_pills_used++;
}

/* adrenalin */
function OnGameEvent_adrenaline_used(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.adrenaline_used++;
}

/* given pills and adrenaline */
function OnGameEvent_weapon_given(data) {
  local giverData = getPlayerData(data.giver);
  local entity = EntIndexToHScript(data.weaponentid);

  if ( giverData == null || entity == null ) {
    return;
  }

  local entityName = entity.GetClassname();

  if ( entityName == "weapon_pain_pills" ) {
    giverData.pain_pills_given++;
  }
  else if ( entityName == "weapon_adrenaline" ) {
    giverData.adrenaline_given++;
  }
}

/* revive */
function OnGameEvent_revive_success(data) {
  local reviverData = getPlayerData(data.userid);

  if ( reviverData == null ) {
    return;
  }

  reviverData.revived_team++;
}

/* defibrillator */
function OnGameEvent_defibrillator_used(data) {
  local userData = getPlayerData(data.userid);
  local targetData = getPlayerData(data.subject);

  if ( userData != null ) {
    userData.defib_for_team++;
  }

  if ( targetData != null ) {
    targetData.defib_from_team++;
  }
}

/* level end */
function OnGameEvent_map_transition(data) {
  printl("end realism");

  ::GAME.duration += Time();
  saveData();

  printl("saved realism");
}

/* level start */
function OnGameEvent_round_start_post_nav(data) {
  printl("start realism");
  loadData();
  printl("loaded realism");
}

/* mission fail */
function OnGameEvent_mission_lost(data) {
  ::GAME.retries++;
  saveData();
}

/* protected and saved */
function OnGameEvent_award_earned(data) {
  /* award id for protected */
  if ( data.award == 67 || data.award == 76 ) {
    local protectorData = getPlayerData(data.userid);
    local targetEntity = EntIndexToHScript(data.subjectentid);

    if ( targetEntity == null || !targetEntity.IsPlayer() ) {
      return;
    }

    local targetData = getPlayerData(targetEntity.GetPlayerUserId());

    if ( targetData == null ) {
      return;
    }

    if ( data.award == 67 ) {
      protectorData.protected_team++;
      targetData.protected_by_team++;
    }
    else if ( data.award == 76 ) {
      protectorData.saved_team++;
      targetData.saved_by_team++;
    }

  }
}

/* finale win */
function OnGameEvent_finale_win(data) {
  ::GAME.game_mode = Director.GetGameMode();
  ::GAME.map = data.map_name;
  ::GAME.difficulty = data.difficulty;
  ::GAME.duration = ::GAME.duration + Time();

  local elapsedTime = ::GAME.duration - ::GAME.last_difficulty_change_time;

  ::GAME.difficulty_ratio["diff_" + data.difficulty] += elapsedTime;

  local player = null;

  while ( player = Entities.FindByClassname(player, "player") ) {
    if ( !player.IsSurvivor() ) {
      continue;
    }

    local playerData = getPlayerData(player.GetPlayerUserId());

    if ( !playerData.player.IsDead() && !playerData.player.IsIncapacitated() && !playerData.player.IsHangingFromLedge() ) {
      playerData.survived = true;
      playerData.disconnected = false;
    }
  }

  /* foreach (playerData in ::PLAYERS) {
    if ( playerData.player != null && playerData.player.IsValid() ) {
      playerData.disconnected = false;
    }
  } */

  /* debugData(); */
  exportData();
}

/* disconnected or connected status (not needed for bots) */
function OnGameEvent_player_team(data) {
  if ( data.isbot ) {
    return;
  }

  local playerData = getPlayerData(data.userid, true);

  playerData.is_human = true;

  if ( data.disconnect ) {
    playerData.disconnected = true;
  }
  else {
    playerData.disconnected = false;
  }
}

/* times player caused boomer explosion on players */
function OnGameEvent_boomer_exploded(data) {
  if ( !("attacker" in data) ) {
    return;
  }

  local attackerData = getPlayerData(data.attacker);

  if ( attackerData == null ) {
    return;
  }

  if ( data.splashedbile ) {
    attackerData.triggered_boomer_splash++;
  }
}

/* times boomer puked/exploded on player */
function OnGameEvent_player_now_it(data) {
  if ( !data.by_boomer ) {
    return;
  }

  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  if ( data.exploded ) {
    playerData.exploded_by_boomer++;
  }
  else {
    playerData.puked_by_boomer++;
  }

}

/* times smoker caught player */
function OnGameEvent_tongue_grab(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.pulled_by_smoker++;
}

/* times smoker choked player */
function OnGameEvent_choke_start(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.choked_by_smoker++;
}

/* times player stopped smoker pull */
function OnGameEvent_tongue_pull_stopped(data) {
  if ( !("userid" in data) ) {
    return;
  }

  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.stopped_smoker_pull++;
}

/* times player stopped smoker choke */
function OnGameEvent_choke_stopped(data) {
  if ( !("userid" in data) ) {
    return;
  }

  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.stopped_smoker_choke++;
}

/* times hunter pounced on player */
function OnGameEvent_lunge_pounce(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.pounced_by_hunter++;
}

/* times player stopped hunger pounce */
function OnGameEvent_pounce_stopped(data) {
  if ( !("userid" in data) ) {
    return;
  }

  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return;
  }

  playerData.stopped_hunter_pounce++;
}

/* times jockey rode player */
function OnGameEvent_jockey_ride(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.rode_by_jockey++;
}

/* times jockey ride was stopped */
function OnGameEvent_jockey_ride_end(data) {
  if ( !("rescuer" in data) ) {
    return;
  }

  local rescuerData = getPlayerData(data.rescuer);

  if ( rescuerData == null ) {
    return;
  }

  rescuerData.killed_riding_jockey++;
}

/* rammed by charger */
function OnGameEvent_charger_carry_start(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.rammed_by_charger++;
}

/* hit by charger */
function OnGameEvent_charger_impact(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.hit_by_charger++;
}

/* pummelled by charger */
function OnGameEvent_charger_pummel_start(data) {
  local victimData = getPlayerData(data.victim);

  if ( victimData == null ) {
    return;
  }

  victimData.pummelled_by_charger++;
}

/* killed a charging charger */
function OnGameEvent_charger_killed(data) {
  if ( !("attacker" in data) ) {
    return;
  }

  local attackerData = getPlayerData(data.attacker);

  if ( attackerData == null ) {
    return;
  }

  if ( data.charging ) {
    attackerData.killed_charging_charger++;
  }
}

/* upgrade pack deployed */
function OnGameEvent_upgrade_pack_used(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return false;
  }

  local upgradeEntity = EntIndexToHScript(data.upgradeid);

  local upgrade = upgradeEntity.GetClassname();

  if ( upgrade == "upgrade_ammo_explosive" ) {
    playerData.deployed_explosive++;
  }
  else if ( upgrade == "upgrade_ammo_incendiary" ) {
    playerData.deployed_incendiary++;
  }
}

/* upgrade pack received */
function OnGameEvent_receive_upgrade(data) {
  local playerData = getPlayerData(data.userid);

  if ( playerData == null ) {
    return false;
  }

  if ( data.upgrade == "EXPLOSIVE_AMMO" ) {
    playerData.upgrades.explosive++;
  }
  else if ( data.upgrade == "INCENDIARY_AMMO" ) {
    playerData.upgrades.incendiary++;
  }
  else if ( data.upgrade == "LASER_SIGHT" ) {
    playerData.upgrades.laser++;
  }
}


function OnGameEvent_difficulty_changed(data) {
  foreach (key, value in data) {
    printl(key + ": " + value);
  }

  printl(::GAME.difficulty_ratio["diff_" + data.oldDifficulty]);

  local currentTime = Time() + ::GAME.duration;
  local elapsedTime = currentTime - ::GAME.last_difficulty_change_time;

  ::GAME.difficulty_ratio["diff_" + data.oldDifficulty] += elapsedTime;
  ::GAME.last_difficulty_change_time = currentTime;
}

function InterceptChat(message, speaker) {
  /* local tmp = {
    map_name = "some_map",
    difficulty = 0
  };

  OnGameEvent_finale_win(tmp); */
  /* debugData(); */
}




/* function OnGameEvent_player_jump(data) {
  printl("hello");
  debugData();
  exportData();
} */

function debugData() {
  foreach (key, value in ::GAME) {
    if ( key == "difficulty_ratio" ) {
      foreach (key2, value2 in value) {
        printl(key2 + ": " + value2);
      }
    }
    else {
      printl(key + ": " + value);
    }
  }

  printl("");

  foreach (key, value in ::PLAYERS) {
    printl(key);
    if ( typeof value == "table" ) {
      foreach (key2, value2 in value) {
        printl(key2 + ": " + value2);

        if ( key2 == "weapons" ) {
          foreach (key3, value3 in value2) {
            printl(key3 + ": " + value3.kills + " (" + value3.hits + "/" + value3.shots + ")");
          }
        }
        else if ( key2 == "incapacitators" || key2 == "upgrades" ) {
          foreach (key3, value3 in value2) {
            printl(key3 + ": " + value3);
          }
        }
      }

      printl("");
    }

  }
}

function saveData() {
  foreach(playerData in ::PLAYERS) {
    playerData.player = {};
  }

  SaveTable(::SAVE_KEYS.GAME, ::GAME);
  SaveTable(::SAVE_KEYS.PLAYERS, ::PLAYERS);
}

function loadData() {
  local gameTable = {};
  local playersTable = {};

  local properGameTable = {};
  local properPlayersTable = {};

  RestoreTable(::SAVE_KEYS.GAME, gameTable);
  RestoreTable(::SAVE_KEYS.PLAYERS, playersTable);

  if ( playersTable.len() > 0 ) {
    foreach (playerName, playerData in playersTable) {
      local cleanPlayerData = {
        weapons = {},
        incapacitators = {},
        player = {},
        upgrades = {}
      };

      foreach (statName, stat in playerData ) {
        if ( typeof stat != "table" ) {
          cleanPlayerData[statName.tolower()] <- stat;
        }
      }

      foreach (weaponName, weaponStats in playerData.weapons) {
        cleanPlayerData.weapons[weaponName.tolower()] <- weaponStats;
      }

      foreach (incapacitator, incapStat in playerData.incapacitators) {
        cleanPlayerData.incapacitators[incapacitator.tolower()] <- incapStat;
      }

      foreach (upgrade, upgradeStat in playerData.upgrades) {
        cleanPlayerData.upgrades[upgrade.tolower()] <- upgradeStat;
      }

      properPlayersTable[playerName] <- cleanPlayerData;
    }

    ::PLAYERS = properPlayersTable;
  }

  if ( gameTable.len() > 0 ) {
    local properGameTable = {
      difficulty_ratio = {}
    };

    foreach (key, value in gameTable ) {
      if ( typeof value != "table" ) {
        properGameTable[key.tolower()] <- value;
      }
    }

    foreach (key, value in gameTable.difficulty_ratio) {
      properGameTable.difficulty_ratio[key.tolower()] <- value;
    }

    ::GAME = properGameTable;
  }
}


function exportData() {
  local matchCode = format("%08i_%08i", RandomInt(1,99999999), RandomInt(1,99999999));;

  local gameDataJson = format(@"
    {
      ""match_code"": ""%s"",
      ""map"": ""%s"",
      ""game_mode"": ""%s"",
      ""difficulty"": %i,
      ""duration"": %f,
      ""retries"": %i,
      ""difficulty_time"": {
        ""diff_0"": %f,
        ""diff_1"": %f,
        ""diff_2"": %f,
        ""diff_3"": %f
      },
      ""is_realism"": %s
    }
  ",
    matchCode,
    ::GAME.map,
    ::GAME.game_mode,
    ::GAME.difficulty,
    ::GAME.duration,
    ::GAME.retries,
    ::GAME.difficulty_ratio.diff_0,
    ::GAME.difficulty_ratio.diff_1,
    ::GAME.difficulty_ratio.diff_2,
    ::GAME.difficulty_ratio.diff_3,
    ::GAME.is_realism.tostring()
  );

  /* difficulty_ratio = {
    diff_0 = 0,
    diff_1 = 0,
    diff_2 = 0,
    diff_3 = 0
  }, */

  local playerDataJson = "[";
  local playerIndex = 1;

  foreach (steamId, playerData in ::PLAYERS) {
    local weaponDataJson = "";
    local weaponIndex = 1;

    foreach (weaponName, weaponStats in playerData.weapons) {
      local comma = ",";

      if ( weaponIndex == playerData.weapons.len() ) {
        comma = "";
      }

      local weaponString = format(@"""%s"": {
        ""kills"": %i,
        ""shots"": %i,
        ""hits"": %i,
        ""headshot_kills"": %i
      }%s", weaponName, weaponStats.kills, weaponStats.shots, weaponStats.hits, weaponStats.headshot_kills, comma);

      weaponDataJson += weaponString;
      weaponIndex++;
    }

    local comma = ",";

    if ( playerIndex == ::PLAYERS.len() ) {
      comma = "";
    }

    local playerString = format(@"{
      ""steam_id"": ""%s"",
      ""witch_damage"": %i,
      ""witch_crown"": %i,
      ""witch_startled"": %i,
      ""deaths"": %i,
      ""deaths_suicide"": %i,
      ""kills_common_infected"": %i,
      ""kills_smoker"": %i,
      ""kills_boomer"": %i,
      ""kills_hunter"": %i,
      ""kills_spitter"": %i,
      ""kills_jockey"": %i,
      ""kills_charger"": %i,
      ""kills_survivor"": %i,
      ""kills_witch"": %i,
      ""kills_tank"": %i,
      ""tank_damage"": %i,
      ""headshot_kills"": %i,
      ""weapon_shots"": %i,
      ""weapon_hits"": %i,
      ""damage_taken"": %i,
      ""times_incapacitated"": %i,
      ""triggered_boomer_splash"": %i,
      ""puked_by_boomer"": %i,
      ""exploded_by_boomer"": %i,
      ""pulled_by_smoker"": %i,
      ""choked_by_smoker"": %i,
      ""stopped_smoker_pull"": %i,
      ""stopped_smoker_choke"": %i,
      ""pounced_by_hunter"": %i,
      ""stopped_hunter_pounce"": %i,
      ""rode_by_jockey"": %i,
      ""killed_riding_jockey"": %i,
      ""rammed_by_charger"": %i,
      ""hit_by_charger"": %i,
      ""pummelled_by_charger"": %i,
      ""killed_charging_charger"": %i,
      ""upgrades"": {
        ""incendiary"": %i,
        ""explosive"": %i,
        ""laser"": %i
      },
      ""deployed_incendiary"": %i,
      ""deployed_explosive"": %i,
      ""incapacitators"": {
        ""common_infected"": %i,
        ""smoker"": %i,
        ""boomer"": %i,
        ""hunter"": %i,
        ""spitter"": %i,
        ""jockey"": %i,
        ""charger"": %i,
        ""witch"": %i,
        ""tank"": %i,
        ""team"": %i,
        ""self"": %i
      },
      ""heal_self"": %i,
      ""heal_for_team"": %i,
      ""heal_from_team"": %i,
      ""damage_friendly_fire"": %i,
      ""damage_friendly_fire_taken"": %i,
      ""times_friendly_fire"": %i,
      ""pain_pills_given"": %i,
      ""pain_pills_used"": %i,
      ""adrenaline_given"": %i,
      ""adrenaline_used"": %i,
      ""revived_team"": %i,
      ""defib_for_team"": %i,
      ""defib_from_team"": %i,
      ""protected_team"": %i,
      ""protected_by_team"": %i,
      ""saved_team"": %i,
      ""saved_by_team"": %i,
      ""survived"": %s,
      ""disconnected"": %s,
      ""is_human"": %s,
      ""character"": ""%s"",
      ""username"": ""%s"",
      ""weapons"": { %s }
    }%s", steamId,
      playerData.witch_damage,
      playerData.witch_crown,
      playerData.witch_startled,
      playerData.deaths,
      playerData.deaths_suicide,
      playerData.kills_common_infected,
      playerData.kills_smoker,
      playerData.kills_boomer,
      playerData.kills_hunter,
      playerData.kills_spitter,
      playerData.kills_jockey,
      playerData.kills_charger,
      playerData.kills_survivor,
      playerData.kills_witch,
      playerData.kills_tank,
      playerData.tank_damage,
      playerData.headshot_kills,
      playerData.weapon_shots,
      playerData.weapon_hits,
      playerData.damage_taken,
      playerData.times_incapacitated,
      playerData.triggered_boomer_splash,
      playerData.puked_by_boomer,
      playerData.exploded_by_boomer,
      playerData.pulled_by_smoker,
      playerData.choked_by_smoker,
      playerData.stopped_smoker_pull,
      playerData.stopped_smoker_choke,
      playerData.pounced_by_hunter,
      playerData.stopped_hunter_pounce,
      playerData.rode_by_jockey,
      playerData.killed_riding_jockey,
      playerData.rammed_by_charger,
      playerData.hit_by_charger,
      playerData.pummelled_by_charger,
      playerData.killed_charging_charger,
      playerData.upgrades.incendiary,
      playerData.upgrades.explosive,
      playerData.upgrades.laser,
      playerData.deployed_incendiary,
      playerData.deployed_explosive,
      playerData.incapacitators.common_infected,
      playerData.incapacitators.smoker,
      playerData.incapacitators.boomer,
      playerData.incapacitators.hunter,
      playerData.incapacitators.spitter,
      playerData.incapacitators.jockey,
      playerData.incapacitators.charger,
      playerData.incapacitators.witch,
      playerData.incapacitators.tank,
      playerData.incapacitators.team,
      playerData.incapacitators.self,
      playerData.heal_self,
      playerData.heal_for_team,
      playerData.heal_from_team,
      playerData.damage_friendly_fire,
      playerData.damage_friendly_fire_taken,
      playerData.times_friendly_fire,
      playerData.pain_pills_given,
      playerData.pain_pills_used,
      playerData.adrenaline_given,
      playerData.adrenaline_used,
      playerData.revived_team,
      playerData.defib_for_team,
      playerData.defib_from_team,
      playerData.protected_team,
      playerData.protected_by_team,
      playerData.saved_team,
      playerData.saved_by_team,
      playerData.survived.tostring(),
      playerData.disconnected.tostring(),
      playerData.is_human.tostring(),
      playerData.character,
      playerData.username,
      weaponDataJson,
      comma
    );

    playerDataJson += playerString;
    playerIndex++;
  }

  playerDataJson += "]";

  local dataJson = format(@"{""game"": %s, ""players"":%s}", gameDataJson, playerDataJson);



  StringToFile(matchCode+".json", dataJson);
}
